package ru.dashko.message;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;

public class MessageServer {
    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(80)) {
            Socket socket = serverSocket.accept();
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Charset.forName("UTF-8")));
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), Charset.forName("UTF-8")));
            String resultString;
            do {
                resultString = in.readLine();
                System.out.println(resultString);
                out.write(resultString + "\n");
                out.flush();
            } while (!resultString.contains("выход"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}