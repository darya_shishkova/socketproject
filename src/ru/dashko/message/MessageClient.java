package ru.dashko.message;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Scanner;

public class MessageClient {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) throws IOException {
        try (Socket socket = new Socket("localhost", 80)) {
            String string;
            String resultString;
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Charset.forName("UTF-8")));
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), Charset.forName("UTF-8")));
            do {
                string = scanner.nextLine() + "\n";
                out.write(string);
                out.flush();
                resultString = in.readLine();
                System.out.println(resultString);
            } while (!resultString.contains("выход"));
        }
    }
}
